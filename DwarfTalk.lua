DwarfTalk = {}

DwarfTalk_Setup =
{
	["OUT"] = true,
}

DwarfTalk_DisabledChannels =
{
	
}

local OriginalOnKeyFunction = nil

function DwarfTalk.OnInitialize()
    EA_ChatWindow.Print (L"DwarfTalk loaded successfully. Type '/dwarf' for options.")
	LibSlash.RegisterSlashCmd ("dwarf", function(input) DwarfTalk.HandleSlashCmd (input) end);
	OriginalUpdateCurrentChannel = EA_ChatWindow.UpdateCurrentChannel
	OriginalOnKeyFunction = EA_ChatWindow.OnKeyEnter
	EA_ChatWindow.UpdateCurrentChannel = DwarfTalk.UpdateCurrentChannel
	EA_ChatWindow.OnKeyEnter = DwarfTalk.Outgoing
end

function DwarfTalk.OnShutdown()
	EA_ChatWindow.OnKeyEnter = OriginalOnKeyFunction
	EA_ChatWindow.UpdateCurrentChannel = OriginalUpdateCurrentChannel
end

function DwarfTalk.HandleSlashCmd (textrest)

	if textrest == "on" then
		DwarfTalk_Setup["OUT"] = true
		EA_ChatWindow.Print (L"Outgoing translation is now ON")
		return;

	elseif textrest == "off" then
		DwarfTalk_Setup["OUT"] = false
		EA_ChatWindow.Print (L"Outgoing translation is now OFF")
		return;
	
	elseif textrest == "help" then
		EA_ChatWindow.Print (L"'/dwarf on' -- turn on translation (except in disabled channels)")
		EA_ChatWindow.Print (L"'/dwarf off' -- turn off translation globally\n")
		EA_ChatWindow.Print (L"'/dwarf disablechan' -- prevents text from being translated in the current channel")
		EA_ChatWindow.Print (L"'/dwarf enablechan' -- undoes effect from '/dwarf disablechan'")
		EA_ChatWindow.Print (L"Include a ' ' (space) at the beginning of individual messages to skip translation.")
	    EA_ChatWindow.Print (L"Include an '_' (underscore) at the beginning of individual messages to force translation.")
		return;
	
	elseif textrest == "disablechan" then	
		if (activeChannel ~= nil and activeChannel ~= "") then
			DwarfTalk_DisabledChannels [activeChannel] = true
			EA_ChatWindow.Print (L"Didn't appreciate your accent did they? Current channel added to disabled list.")
			return;
		else
			EA_ChatWindow.Print (L"DwarfTalk Error: Channel not recognized.")
		end

    elseif textrest == "enablechan" then
		if (activeChannel ~= nil and activeChannel ~= "") then
			if (DwarfTalk_DisabledChannels [activeChannel]) then
				EA_ChatWindow.Print (L"Translation for the current channel was disabled. Enabling.")
				DwarfTalk_DisabledChannels [activeChannel] = false
				--table.remove (DwarfTalk_DisabledChannels, activeChannel)
			else
				EA_ChatWindow.Print (L"DwarfTalk Error: Translation for the current channel is not currently disabled. Doing nothing.")
			end
			return;
		else
			EA_ChatWindow.Print (L"DwarfTalk Error: Channel not recognized.")
		end
		
	elseif (DwarfTalk_Setup["OUT"] == true) then
        EA_ChatWindow.Print (L"Outgoing translation is currently ON.\nType '/dwarf help' for a list of commands.")
	else
	    EA_ChatWindow.Print (L"Outgoing translation is currently OFF.\nType '/dwarf help' for a list of commands.")
	end
end

function DwarfTalk.UpdateCurrentChannel(icurChannel)
	activeChannel = icurChannel
	OriginalUpdateCurrentChannel(icurChannel)
end

function DwarfTalk.Outgoing (...)

	d (L"DwarfTalk called!")

	local sinput = WStringToString (EA_TextEntryGroupEntryBoxTextInput.Text)

    if (sinput:sub(1,1) ~= "_") then
    
		if (sinput == "" or sinput == nil or sinput:sub(1,1) == "/" or sinput:sub(1,1) == "]" or sinput:sub(1,1) == "*" or DwarfTalk_Setup["OUT"] ~= true) then
			return OriginalOnKeyFunction (...)
		end
		
		if (sinput:sub(1,1) == " ") then
			EA_TextEntryGroupEntryBoxTextInput.Text = towstring (sinput:sub (2, -1))
			return OriginalOnKeyFunction (...)
		end
		
		if (sinput:sub(1,1) == "(" and sinput:sub(2,2) == "(") then
			return OriginalOnKeyFunction (...)
		end
		
		--at this point /dwarf is set to on and we've been given legitimate chat text to translate
	
		if (activeChannel ~= nil and activeChannel ~= "") then --we have a channel
			if (DwarfTalk_DisabledChannels [activeChannel]) then   --but this is a disabled channel
				return OriginalOnKeyFunction (...)		
			end
		end
	
    	--if activeChannel == SystemData.ChatLogFilters.SAY then
    	--	EA_ChatWindow.Print (L"the channel sent to is SAY")
    	--elseif activeChannel == SystemData.ChatLogFilters.GUILD then
    	--	EA_ChatWindow.Print (L"the channel sent to is GUILD")
			--ChatWindowContainerTextInput.Text = L"/2 "..inputText
		--end     
	end
	
	if (sinput:sub(1,1) == "_") then
		sinput = sinput:sub (2, -1)
	end

	local translated = ""	
	translated = DwarfTalkEN (sinput)
	EA_TextEntryGroupEntryBoxTextInput.Text = towstring (translated)

	return OriginalOnKeyFunction (...)
end