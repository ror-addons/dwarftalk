DwarfTalk V1 Notes:
-----------------

This addon takes your non-command chat and translates it into Dwarven speak, or at least into an Irish/piratey-accented form of English that resembles what an English-speaking Dwarf would be expected to say. You can turn this on and off on the fly without removing the addon. You can also choose to only translate specific sentences you write.

This addon requires the LibSlash addon to function.

- To prevent an individual line of text from being translated, put either a ' ' (space) or '((' (double parentheses) before the text.
- To force translation on a line of text while translation is disabled, put an '_' (underscore) before the text.

The different commands:

/dwarf on
Turns translation mode on. All text that isn't preceded by special characters, a space, double parentheses, or occurs in a "disabled" channel will be translated.

/dwarf off
Turns off translation mode. No text will be translated unless preceded by an '_" (underscore).

/dwarf disablechan
Prevents text from being translated in the current channel. For example, to discontinue translating in guild chat, make sure your current channel is guild chat (either by saying something in guild chat first, or by typing out '/guild' before typing out '/dwarf disablechan'

/dwarf enablechan
Undoes effect from disablechan. As with disabling, you must set your current channel to the channel you intend to allow translation on again.


Original credit goes to tmpst of the original German/English "Orcanizer" addon, and to Shryke of the redone OrcTalk addon.

~Skagrotho Mintbeard the Fine Fuser, Engineer of Chaos Wastes, Clans Dwarf of Hammer and Anvil


Changes
----------
1.1.2
-----
-Fixed a bug where words with certain endings would not translate correctly if done completely in uppercase

-Cleaned up some code

-More vocabulary changes.

1.1.1
-----
-Capitalization is preserved for the first letters of each word, as well as all letters when all are capitalized.

-OOC '((' '))' text will no longer be translated.

-Added more vocabulary.

1.1
-----
-Translation can be toggled for separate channels.

-Added a lot of new vocabulary and a few grammatical fixes.

1.0.1
-----
-Fixed a bug that would invariably send messages to /say if the translation began with an apostrophe.

-Added a lot of new vocabulary.

