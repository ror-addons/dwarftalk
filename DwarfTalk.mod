<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="DwarfTalk" version="1.1.2" date="10.19.2008" >
		<Author name="Skagrotho" />
		<Description text="Converts English to Dwarven speak." />
		<Dependencies>
			<Dependency name="EA_ChatWindow" />
			<Dependency name="LibSlash" />
		</Dependencies>
		<Files>
			<File name="DwarfTalk.lua" />
			<File name="Translate.lua" />
		</Files>
		<SavedVariables>
			<SavedVariable name="DwarfTalk_Setup" />
			<SavedVariable name="DwarfTalk_DisabledChannels" />
		</SavedVariables>
		<OnInitialize>
			<CallFunction name="DwarfTalk.OnInitialize" />
		</OnInitialize>
		<OnUpdate />
		<OnShutdown>
			<CallFunction name="DwarfTalk.OnShutdown" />
		</OnShutdown>
	</UiMod>
</ModuleFile>
